import asyncio
import functools
import logging
import pathlib
import socket
import time
from collections.abc import Callable
from datetime import timedelta
from typing import Any

import cloudpickle
import conda_pack
import skein


def pack_conda_env(name: str | None = None) -> pathlib.Path:
    if name:
        conda_env_path = conda_pack.CondaEnv.from_name(name).pack(
            force=True, verbose=True
        )
    else:
        conda_env_path = conda_pack.CondaEnv.from_default().pack(
            force=True, verbose=True, output="env.tgz"
        )
    return pathlib.Path(conda_env_path)


class YarnSession:
    """Experimental session for running functions remotely on YARN."""

    def __init__(
        self,
        name: str,
        cores: int,
        memory_mb: int,
        files: dict[str, str] | None = None,
        conda_env: str | None = None,
        queue: str = "default",
        node_label: str = "",
    ) -> None:
        self._name = name
        self._cores = cores
        self._memory_mb = memory_mb
        self._files = files
        self._conda_env = conda_env
        self._queue = queue
        self._node_label = node_label

        self.logger = logging.getLogger(__name__)
        logging.basicConfig(
            level=logging.INFO,
            format="%(asctime)s %(levelname)s %(name)s: %(message)s",
            datefmt="%y/%m/%d %H:%M:%S",
        )

        is_server_ready = self._is_server_ready(timeout=timedelta(seconds=5 * 60))
        if is_server_ready:
            self.logger.info("Server launched successfully.")
        else:
            raise RuntimeError(
                "Server took too long to launch. "
                "Killing this session and starting a new one might "
                "resolve the issue or try looking at the logs:\n"
                f"$ yarn logs -applicationId {self._app.id}"
            )

    @functools.cached_property
    def _skein_spec(self) -> skein.ApplicationSpec:
        server_script = pathlib.Path(__file__).resolve().parent / "server.py"
        conda_env_path = pack_conda_env(name=self._conda_env)
        additional_files = self._files or {}
        session_service = skein.Service(
            resources=skein.Resources(vcores=self._cores, memory=self._memory_mb),
            script="source conda_env/bin/activate && python3 server.py",
            files=(
                {"conda_env": str(conda_env_path), "server.py": str(server_script)}
                | additional_files
            ),
        )
        return skein.ApplicationSpec(
            name=self._name,
            queue=self._queue,
            node_label=self._node_label,
            services={self._name: session_service},
        )

    @functools.cached_property
    def _app(self) -> skein.ApplicationClient:
        self.logger.info("Submitting skein application...")
        with skein.Client() as client:
            app_id = client.submit(self._skein_spec)
            app = client.connect(app_id)
            [container] = app.get_containers(services=(self._name,))

            self.logger.info(
                "Waiting for skein service to run. This could take a few seconds."
            )
            while container.state != "RUNNING":
                time.sleep(0.5)
                [container] = app.get_containers(services=(self._name,))

        return app

    @functools.cached_property
    def _host(self) -> str:
        [container] = self._app.get_containers(services=(self._name,))
        container_addr, *_ = container.yarn_node_http_address.partition(":")
        return container_addr

    @functools.cached_property
    def _port(self) -> int:
        for _key, port in self._app.kv.get_prefix("port.").items():
            return int.from_bytes(port, "big")

        raise LookupError("Could not retrieve port that remote server is listening on.")

    def _is_server_ready(self, timeout: timedelta = timedelta(seconds=60 * 3)) -> bool:
        start = time.time()
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            while time.time() < start + timeout.total_seconds():
                try:
                    sock.connect((self._host, self._port))
                    return True
                except (LookupError, OSError):
                    time.sleep(1)

        return False

    async def _run_on_server(
        self, func: Callable[..., Any], args: Any, kwargs: Any
    ) -> Any:
        reader, writer = await asyncio.open_connection(host=self._host, port=self._port)

        serialized_func = cloudpickle.dumps((func, args, kwargs))
        writer.write(serialized_func)
        writer.write_eof()

        data = await reader.read()
        result = cloudpickle.loads(data)
        writer.close()

        if isinstance(result, Exception) or (
            isinstance(result, type) and issubclass(result, Exception)
        ):
            raise result

        return result

    def function(self, timeout: timedelta = timedelta(minutes=5)) -> Callable[..., Any]:
        def decorator(func: Callable[..., Any]) -> Callable[..., Any]:
            @functools.wraps(func)
            async def wrapper(*args: Any, **kwargs: Any) -> Any:
                future = self._run_on_server(func, args, kwargs)
                try:
                    result = await asyncio.wait_for(
                        fut=future, timeout=timeout.total_seconds()
                    )
                    return result
                except asyncio.TimeoutError:
                    raise TimeoutError(
                        f"Function did not finish running in {timeout.total_seconds()}. "
                        "This could be an issue with the server or your function is still running."
                    )

            return wrapper

        return decorator

    def stop(self) -> None:
        with skein.Client() as client:
            client.kill_application(self._app.id)

import asyncio
import logging

import cloudpickle
import skein


async def _handle_func(
    reader: asyncio.StreamReader, writer: asyncio.StreamWriter
) -> None:
    logging.info("Handling func...")

    data = await reader.read()
    deserialized_func = cloudpickle.loads(data)
    logging.info("Deserialized function.")
    func, args, kwargs = deserialized_func

    try:
        result = func(*args, **kwargs)
    except Exception as err:
        result = err

    logging.info("Function result: %r", result)
    serialized_result = cloudpickle.dumps(result)
    logging.info("Serialized result.")

    writer.write(serialized_result)
    await writer.drain()
    logging.info("Sent result.")

    writer.close()
    await writer.wait_closed()


async def main() -> None:
    server = await asyncio.start_server(
        client_connected_cb=_handle_func, host="0.0.0.0"
    )
    [socket] = server.sockets
    _host, port = socket.getsockname()

    with skein.ApplicationClient.from_current() as client:
        key = f"port.{skein.properties.container_id}"
        value = port.to_bytes(length=2, byteorder="big")
        client.kv.put(key, value, owner=skein.properties.container_id)

    logging.info("Starting server...")
    async with server:
        await server.serve_forever()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    asyncio.run(main())

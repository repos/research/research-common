import hashlib
from io import BytesIO
from PIL import Image
from pyspark.sql import Row
from research_common.swift.swift import decode_b64

# file estimates from small exeriments

# 400px
mb_per_image_400px = 61.8e3/1e6
# 300px
mb_per_image_300px = 39.4e3/1e6

# measured with 400px
mb_per_second_per_thread = 61.8e3/(4.6*60*60)/(20*4)

def open_image(image_bytes):
    """returns a PIL.Image from bytes"""
    return Image.open(BytesIO(image_bytes))

image_schema="""
struct<
    image_bytes_b64:string,
    format:string,
    width:int,
    height:int,
    image_bytes_sha1:string,
    error:string
>
"""

@F.udf(returnType=image_schema)
def parse_image(image_bytes_b64):
    try:
        image_bytes = decode_b64(image_bytes_b64)
        image = open_image(image_bytes)
        image_bytes_sha1 = hashlib.sha1(image_bytes).hexdigest()
        return Row(
            image_bytes_b64=image_bytes_b64,
            format=image.format,
            width=image.width,
            height=image.height,
            image_bytes_sha1=image_bytes_sha1,
            error=None
        )
    except Exception as e:
        print(f"Error parsing image bytes. Exception: {e}")
        return Row(
            image_bytes_b64=image_bytes_b64,
            format=None,
            width=None,
            height=None,
            image_bytes_sha1=None,
            error=str(e))


import argparse
import logging
from typing import List, Optional, Tuple
import pyspark.sql.functions as F
import pyspark.sql.types as T
import random
from research_common.spark import create_yarn_spark_session
from research_common.swift import swift

# back of the envelope estimates for running time / qps
# estimates for running job
# sec = 5.2*60*60
# mb_total = 37000
# n_files = 971640
# threads = 60

# stats={}
# stats['thumbnail_size_300px'] = mb_total/n_files
# stats['qps'] = n_files/sec
# stats['qps_per_thread'] = stats['qps']/threads

# stats
# {'thumbnail_size_300px': 0.038079947305586434,
#  'qps': 51.90384615384615,
#  'qps_per_thread': 0.8650641025641026}


def download_commons_media(
    spark,
    snapshot, # '2021-08',
    media_output_dir, # ='test',
    wiki_dbs=None, # ['commons'],
    prefixes=['File:'],
    suffixes=None, # ['.wav'] or ['.png', '.jpg', '.jpeg', '.gif'],
    mb_per_file=None, # 0.3
    thumb_size=None, # 400
    partition_size_mb=200,
    swift_download_errors_dir=None):
    """"
    Advisory: the spark context will be used to query the swift cluster.
    The number of threads querying swift will depend on the configuration
    of the spark session, beware.
    """
    # todo special case: no spark allowed to be passed in?
    # spark.stop()
    # spark = swift.create_spark_context_for_swift()

    history = spark.sql(f"""
    select * from wmf.mediawiki_wikitext_current where snapshot='{snapshot}'
    """)

    if wiki_dbs:
        wikis = [f'{db}wiki' for db in wiki_dbs]
        history = history.where(F.col('wiki_db').isin(wikis))


    @F.udf(returnType=T.BooleanType())
    def filter_files(page_title):
        """Return true if we are generally interested in this row"""
        good_start = not prefixes or any(page_title.startswith(prefix) for prefix in prefixes)
        good_end = not suffixes or any(page_title.endswith(suffix) for suffix in suffixes)
        return good_start and good_end

    page_to_filename = F.udf(lambda pt: pt[5:].replace(" ", "_"), 'string')

    history = (history
        .filter(filter_files('page_title'))
        .withColumn("file_name", page_to_filename('page_title'))
        # todo
        .withColumn("project", F.lit('commons'))
        )

    swift.append_image_bytes(
        input_df=history,
        mb_per_file=mb_per_file,
        output_dir=media_output_dir,
        thumb_size=thumb_size,
        partition_size_mb=partition_size_mb,
        swift_download_errors_dir=swift_download_errors_dir)

def prepare_commons_jobs(
        mediawiki_snapshot: str,
        file_objects_per_job: int,
        suffixes: List[str],
        jobs_output_dir: str) -> Tuple[int, int]:
    """
    Split the commons history into smaller datasets to batch the jobs downloading data from swift

    :param mediawiki_snapshot: mediawiki snapshot to process
    :param file_objects_per_job: number of swift file objects to download in a job
    :param suffixes: file extenses to download file objects for
    :param jobs_output_dir: hdfs directory to store smaller datasets, partitioned by job id
    """
    conf =  {
        "spark.dynamicAllocation.maxExecutors": 128,
        "spark.executor.cores": 8,
        "spark.sql.shuffle.partitions": 128,
    }
    prepare_spark = create_yarn_spark_session(app_id="prepare_commons_jobs", extra_config=conf)

    history = (prepare_spark
        .sql(f"select * from wmf.mediawiki_wikitext_current where snapshot='{mediawiki_snapshot}'")
        .where(F.col('wiki_db')=='commonswiki')
    )

    prefixes=['File:']
    @F.udf(returnType="boolean")
    def filter_files(page_title):
        """Return true if we are generally interested in this row"""
        good_start = not prefixes or any(page_title.startswith(prefix) for prefix in prefixes)
        good_end = not suffixes or any(page_title.endswith(suffix) for suffix in suffixes)
        return good_start and good_end

    # go from File:Siembra.png -> Siembra.png
    page_to_filename = F.udf(lambda pt: pt[5:].replace(" ", "_"), 'string')
    # go from commonswiki -> commons
    wiki_db_to_wiki = F.udf(lambda wdb: wdb[:-4], 'string')

    history = (history
        .filter(filter_files('page_title'))
        .withColumn("file_name", page_to_filename('page_title'))
        .withColumn("project", wiki_db_to_wiki('wiki_db'))
    )

    # count number of files, this is a map/reduce job
    total_file_objects = history.count()
    # compute the number datasets to create, one for each job
    num_jobs = int(total_file_objects /  file_objects_per_job) + 1

    @F.udf(returnType='string')
    def assign_job_id():
        return f'job_{random.randint(0, num_jobs-1)}'

    # assign each file object to a random job_id and write the sub datasets
    # partitioned by job_id
    (history
        .withColumn("job_id",assign_job_id())
        .write
        .mode("overwrite")
        .partitionBy("job_id")
        .format("avro")
        .save(jobs_output_dir)
    )

    prepare_spark.stop()

    return (total_file_objects, num_jobs)

def run_job(
        swift_spark,
        job_id: int,
        n_threads_querying_swift: int,
        input_dir: str,
        files_output_dir: str,
        errors_output_dir: str,
        thumb_size: Optional[int]=None):
    """
    Downloads the file objects from swift.

    :param job_id: used for the spark context
    :param n_threads_querying_swift: number of threads querying the swift cluster
    :param input_dir: hdfs directory containing input data, prepared by `prepare_commons_jobs`
    :param files_output_dir: hdfs output directory for the file objects
        successfully downloaded from swift
    :param errors_output_dir: hdfs output directory for the errors that occured

    """
    logging.info(f'running job {job_id}')
    logging.info(f'downloading from swift with {n_threads_querying_swift} threads')
    logging.info(f'input data dir: {input_dir}')
    logging.info(f'files output dir: {files_output_dir}')
    logging.info(f'errors output dir: {errors_output_dir}')
    # using a single core per executor to spread the network load across executors
    # conf =  {
    #     "spark.dynamicAllocation.maxExecutors": n_threads_querying_swift,
    #     "spark.executor.cores": 1,
    # }
    # swift_spark = create_yarn_spark_session(app_id=f'commons_images_job_{job_id}',extra_config=conf)

    input_df = swift_spark.read.format("avro").load(input_dir)
    files, errors = swift.download_files(input_df, n_threads_querying_swift, thumb_size)
    files.write.format("avro").mode("overwrite").save(files_output_dir)
    errors.write.format("avro").mode("overwrite").save(errors_output_dir)

    # swift_spark.stop()
    logging.info(f'completed job {job_id}')


def download_commons_images():
    """
    PYSPARK_PYTHON=$(which python) \
    spark2-submit \
        --master yarn \
        --packages org.apache.spark:spark-avro_2.11:2.4.4 \
        $(which download_commons_images.py) \
        --mediawiki_snapshot 2022-05 \
        --output_dir commons_images_2022-05 \
        --n_threads_querying_swift 120
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--mediawiki_snapshot',
                        required=True,
                        help='Mediawiki snapshot')

    parser.add_argument('--output_dir',
                        required=True,
                        help='HDFS output directory for commons images dataset')

    parser.add_argument('--thumbnail_size',
                        type=int,
                        default=300,
                        help='Thumbnail size to download from swift. Using `None` will download the full images, beware.')

    parser.add_argument('--file_suffixes',
                        type=lambda l: l.split(','),
                        default=['.png', '.jpg', '.jpeg'],
                        help='Comma separated list of suffixes of files to download')

    parser.add_argument('--file_objects_per_job',
                        type=int,
                        default=1000000,
                        help='Number of files to download per sub-job')

    parser.add_argument('--n_threads_querying_swift',
                        type=int,
                        default=60,
                        help='Mediawiki snapshot')


    args = parser.parse_args()


    jobs_output_dir = f"{args.output_dir}/jobs/"
    files_output_dir = f"{args.output_dir}/files/"
    errors_output_dir = f"{args.output_dir}/errors/"

    logging.info(f'downloading commons images with suffixes {args.file_suffixes}')
    logging.info(f'using thumbnail size {args.thumbnail_size}')
    logging.info(f'using snapshot {args.mediawiki_snapshot}')
    logging.info(f'batched jobs data directory: {jobs_output_dir}')
    logging.info(f'files output directory: {files_output_dir}')
    logging.info(f'errors output directory: {files_output_dir}')

    # TODO, print heuristics about expected running time and expected dataset size
    # thumbnail_size gives ~mb_per_file_object object
    # mb_total = file_objects_per_job * mb_per_file_object
    # mb_per_partition = mb_total / n_threads_querying_swift


    # logging.info(f'splitting dataset into batches of {args.file_objects_per_job} files each')
    # total_file_objects, num_jobs = prepare_commons_jobs(
    #     mediawiki_snapshot=args.mediawiki_snapshot,
    #     file_objects_per_job=args.file_objects_per_job,
    #     suffixes=args.file_suffixes,
    #     jobs_output_dir=jobs_output_dir)
    # logging.info(f'batch jobs ready: {num_jobs} will download {total_file_objects} file objects in total')

    # for job_id in
    # range(0, num_jobs):
    #     run_job(
    #         job_id=job_id,
    #         n_threads_querying_swift=args.n_threads_querying_swift,
    #         input_dir=f"{jobs_output_dir}/job_id=job_{job_id}",
    #         files_output_dir=f"{files_output_dir}/job_id=job_{job_id}/",
    #         errors_output_dir=f"{errors_output_dir}/job_id=job_{job_id}/",
    #         thumb_size=args.thumbnail_size
    #     )

    conf =  {
        "spark.dynamicAllocation.maxExecutors": args.n_threads_querying_swift,
        "spark.executor.cores": 1,
    }
    swift_spark = create_yarn_spark_session(app_id=f'commons_images_jobs',extra_config=conf)

    for job_id in range(60, 68):
        run_job(
            swift_spark=swift_spark,
            job_id=job_id,
            n_threads_querying_swift=args.n_threads_querying_swift,
            input_dir=f"{jobs_output_dir}/job_id=job_{job_id}",
            files_output_dir=f"{files_output_dir}/job_id=job_{job_id}/",
            errors_output_dir=f"{errors_output_dir}/job_id=job_{job_id}/",
            thumb_size=args.thumbnail_size
        )


    swift_spark.stop()

if __name__ == '__main__':
    import sys
    sys.exit(download_commons_images())


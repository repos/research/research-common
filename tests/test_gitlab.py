from research_common import gitlab
import pytest

def test_no_dev_release_for_branches():
    with pytest.raises(AssertionError) as e_info:
        gitlab.latest_package_file("",dev=True,version="xyz")

#gitlab integration tests

@pytest.fixture
def research_common_repo_fn():
    def call_fn(**kargs):
        kargs["project_path"] = "repos/research/research-common"
        return gitlab.latest_package_file(**kargs)
    return call_fn

def test_gitlab_ci_dev_release(research_common_repo_fn):
    link = research_common_repo_fn(dev=True)
    assert "dev" in link, f"requested dev package, received {link}"

def test_gitlab_ci_no_package(research_common_repo_fn):
    with pytest.raises(ValueError) as e_info:
        research_common_repo_fn(version="0.0.0", dev=True)

    with pytest.raises(ValueError) as e_info:
        research_common_repo_fn(version="0.0.0")

    with pytest.raises(ValueError) as e_info:
        research_common_repo_fn(version="non_existent_branch")

def test_gitlab_ci_semantic_version(research_common_repo_fn):

    version = "0.1.0"
    with pytest.raises(ValueError) as e_info:
        research_common_repo_fn(dev=True,version=version)

    link = research_common_repo_fn(dev=False,version=version)
    assert version in link, f"requested version {version}, received {link}"

    version = "0.1.1"
    link = research_common_repo_fn(dev=True,version=version)
    assert version in link, f"requested version {version}, received {link}"
